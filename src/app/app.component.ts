import {Component, OnInit} from '@angular/core';
import {FileElement} from './file-explorer/model/file-element';
import {FileService} from './services/file.service';
import {combineLatest} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  fileElements: Object;
  currentRoot: FileElement;
  isLoading: boolean;
  params: {
    search: string,
    limit: number,
    page: number,
    total: number
  };
  currentPath: string;
  canNavigateUp = false;

  constructor(public fileService: FileService) {
  }

  ngOnInit() {
    this.params = {
      search: '',
      limit: 100,
      page: 1,
      total: 1000
    };
    this.updateFileElementQuery();
  }

  addFolder(name: string) {
    const payload = {
      name,
      parent: this.currentRoot ? this.currentRoot.id : ''
    };
    this.fileService.addFolder(payload)
      .subscribe(() => this.updateFileElementQuery());
  }

  addDocument(file: File) {
    this.fileService.addDocument(file, this.currentRoot ? this.currentRoot.id : null)
      .subscribe(() => this.updateFileElementQuery());
  }


  updateSearch(search: string) {
    this.params.search = search;
    this.updateFileElementQuery();
  }

  updatePagination(page: { pageIndex: number }) {
    this.params.page = page.pageIndex;
    this.updateFileElementQuery();

  }

  updateFileElementQuery() {
    this.isLoading = true;
    this.fileService.getFolderContent(
      this.params.page,
      this.params.limit,
      this.params.search,
      this.currentRoot ? this.currentRoot.id : null)
      .subscribe(res => {
        this.isLoading = false;
        this.fileElements = res;
      });
  }

  downloadDocument(documentId: string) {
    combineLatest(
      this.fileService.getDocumentMetadata(documentId),
      this.fileService.downloadDocument(documentId)
    )
      .pipe(map(arr => {
        const file: any = arr[0];
        return {
          filename: file.name,
          data: arr[1]
        };
      }))
      .subscribe(res => {
        console.log('start download:', res);
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.');
      });
  }

  navigateUp() {
    if (this.currentRoot && this.currentRoot.parent === '') {
      this.currentRoot = null;
      this.canNavigateUp = false;
      this.updateFileElementQuery();
    } else {
      this.fileService.get(this.currentRoot.parent)
        .subscribe((folder: FileElement) => {
          this.currentRoot = folder;
          this.updateFileElementQuery();
        });
    }
    this.currentPath = this.popFromPath(this.currentPath);
  }

  navigateToFolder(element: FileElement) {
    this.currentRoot = element;
    this.updateFileElementQuery();
    this.currentPath = this.pushToPath(this.currentPath, element.name);
    this.canNavigateUp = true;
  }

  pushToPath(path: string, folderName: string) {
    let p = path ? path : '';
    p += `${folderName}/`;
    return p;
  }

  popFromPath(path: string) {
    let p = path ? path : '';
    let split = p.split('/');
    split.splice(split.length - 2, 1);
    p = split.join('/');
    return p;
  }

}
