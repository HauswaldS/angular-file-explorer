import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {FileElement} from './model/file-element';
import {MatDialog, MatMenuTrigger} from '@angular/material';
import {NewFolderDialogComponent} from './modals/new-folder-dialog/new-folder-dialog.component';
import {RenameDialogComponent} from './modals/rename-dialog/rename-dialog.component';
import {debounceTime} from 'rxjs/operators';
import {distinctUntilChanged} from 'rxjs/internal/operators/distinctUntilChanged';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'app-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.scss'],
})
export class FileExplorerComponent implements OnInit {
  @Input() fileElements: FileElement[];
  @Input() canNavigateUp: string;
  @Input() path: string;
  @Input() isLoading: boolean;

  @Output() searchValueChanged = new EventEmitter<String>();
  @Output() paginationChanged = new EventEmitter<String>();
  @Output() folderAdded = new EventEmitter<{ name: string }>();
  @Output() documentAdded = new EventEmitter<File>();
  @Output() elementRemoved = new EventEmitter<FileElement>();
  @Output() elementRenamed = new EventEmitter<FileElement>();
  @Output() elementMoved = new EventEmitter<{
    element: FileElement
    moveTo: FileElement
  }>();
  @Output() navigatedDown = new EventEmitter<FileElement>();
  @Output() navigatedUp = new EventEmitter();
  @Output() fileDownload = new EventEmitter<string>();

  private searchValue$: Subscription;
  private searchValue = new Subject<string>();

  dropzoneActive: boolean = false;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
    this.searchValue.pipe(
      debounceTime(300),
      distinctUntilChanged()
    );

    this.searchValue$ = this.searchValue.subscribe((search) => this.searchValueChanged.emit(search));
  }

  search(term: string): void {
    this.searchValue.next(term);
  }


  deleteElement(element: FileElement) {
    this.elementRemoved.emit(element);
  }

  navigate(element: FileElement) {
    if (!element.uploadDate) {
      this.navigatedDown.emit(element);
    }
  }

  navigateUp() {
    this.navigatedUp.emit();
  }


  openMenu(event: MouseEvent, element: FileElement, viewChild: MatMenuTrigger) {
    event.preventDefault();
    viewChild.openMenu();
  }

  downloadDocument(element) {
    this.fileDownload.emit(element.id);
  }

  openNewFolderDialog() {
    const dialogRef = this.dialog.open(NewFolderDialogComponent);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.folderAdded.emit(res);
      }
    });
  }

  openRenameDialog(element: FileElement) {
    const dialogRef = this.dialog.open(RenameDialogComponent);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        element.name = res;
        this.elementRenamed.emit(element);
      }
    });
  }


  dropzoneState($event: boolean) {
    this.dropzoneActive = $event;
  }

  handleDrop(fileList: FileList) {
    for (let i = 0; i < fileList.length; i++) {
      this.documentAdded.emit(fileList.item(i));
    }
  }
}
