export class FileElement {
  id?: string;
  name: string;
  parent?: string;
  extension?: string;
  size?: number;
  uploadDate?: Date;
}
