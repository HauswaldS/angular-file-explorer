import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {HttpClient, HttpRequest} from '@angular/common/http';
import {last} from 'rxjs/internal/operators/last';


export interface IFileService {
  addFolder(payload: { name: string, parent: string }): Observable<Object>;

  addDocument(file: File, parentId: string): Observable<Object>;

  get(id: string): Observable<Object>;

  getFolderContent(page: number, limit: number, search: string, folderId?: string): Observable<Object>;

  getDocumentMetadata(documentId: string): Observable<Object>;

  downloadDocument(documentId: string): Observable<Blob>;
}

@Injectable()
export class FileService implements IFileService {
  private root = 'http://localhost:8080/api/ged';

  constructor(private http: HttpClient) {
  }

  addFolder(payload: { name: string, parent: string }) {
    return this.http.post(`${this.root}/folders`, payload);
  }

  addDocument(file: File, parentId: string) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    const req = new HttpRequest(
      'POST',
      `${this.root}/folders/${parentId ? parentId : ''}/documents`,
      formData,
      {reportProgress: true}
    );

    return this.http.request(req).pipe(last());
  }

  get(folderId: string) {
    return this.http.get(`${this.root}/folders/${folderId}`);
  }

  getFolderContent(page: number, limit: number, search: string, folderId?: string) {
    return this.http.get(`${this.root}/folders/${folderId}/all?page=${page}&limit=${limit}&search=${search}`);
  }

  getDocumentMetadata(documentId: string) {
    return this.http.get(`${this.root}/documents/${documentId}/meta`);
  }


  downloadDocument(documentId: string) {
    return this.http.get(`${this.root}/documents/${documentId}`, {
      responseType: 'blob',
    });
  }
}
