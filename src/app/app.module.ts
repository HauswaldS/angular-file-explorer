import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {FileExplorerModule} from './file-explorer/file-explorer.module';
import {AppComponent} from './app.component';
import {FileService} from './services/file.service';
import {MatPaginatorModule} from '@angular/material';
@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FileExplorerModule,
    MatPaginatorModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [FileService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
